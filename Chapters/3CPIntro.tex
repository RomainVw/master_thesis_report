\chapter{Constraint programming} 
\lhead{\vspace{1em}\emph{LINGI2990 - Master Thesis} :: Constraint Programming}
\section{Constraint Satisfaction Problems (CSP)}

A \textbf{Constraint Satisfaction Problem} is a mathematical problem composed of a set of variables (with each variable having its own domain) and a set of constraints expressed over the variables. It is often represented by a triplet $<\mathcal{X}, \mathcal{D}, \mathcal{C}>$, where $\mathcal{X} = \{x_1, \;\dots\;, x_m\}$ is the set of variables, $\mathcal{D} = \{Dom_1, \;\dots\;, Dom_m\}$ is the set of corresponding domains and $\mathcal{C} = \{c_1, \;\dots\;, c_n\}$ is the set of constraints that hold on all those variables. A solution to such a problem is found when all the variables are assigned to a value of their domain so that no constraint is violated.\\

\section{Constraint programming}
\textbf{Constraint Programming} is a programming paradigm  developed from (Constraint) Logic programming. It has been built to search and solve CSP in an efficient and expressive way. As stated in the course notes of Pr. Deville \citep{CPCourseNotes}
\begin{equation}
CP = Search + Model
\end{equation}
Where the model is the description of the problem (the CSP, see chapter \ref{model} for ours) and the search is the techniques and algorithms exploited by the CP paradigm. The search is composed of two main steps :
\begin{itemize}
\item The first step is \textbf{the propagation (or filtering)}. Responsible for the removal of impossible (having no support) values from the domains, it trims down the search space (pruning) until no more constraints can remove values from domains. Let's take a simple example.


\begin{figure}[!t]
\centering
\begin{tikzpicture}[xscale=1,transform shape]


\draw (0,1.5) node[below ]{$x \in \{ 2,3,4,5,6\} \quad y \in \{0,1, 2,3,4\}$};

	\node (source) at (0,1.0){};
       \node (destination) at (0,0){};
       \draw[very thick, ->](source)--(destination);
		\draw (0,0.5) node[anchor=west]{$c_1 \equiv x < y$};

\draw (0,0.0) node[below ]{$x \in \{ 2,3,\textcolor{red}{\cancel{4},\cancel{5},\cancel{6}}\} \quad y \in \{\textcolor{red}{\cancel{0},\cancel{1},\cancel{2}},3,4\}$};

	\node (source) at (0,-0.5){};
       \node (destination) at (0,-1.5){};
       \draw[very thick, ->](source)--(destination);
		\draw (0,-1.0) node[anchor=west]{$c_2 \equiv x > 2$};


\draw (0,-1.5) node[below ]{$x \in \{ \textcolor{red}{\cancel{2}}, 3\} \quad y \in \{3,4\}$};

	\node (source) at (0,-2.0){};
       \node (destination) at (0,-3){};
       \draw[very thick, ->](source)--(destination);
		\draw (0,-2.5) node[anchor=west]{$c_1 \equiv x < y$};

\draw (0,-3.0) node[below ]{$x \in \{ 3 \} \quad y \in \{\textcolor{red}{\cancel{3}},4\}$};



\end{tikzpicture}
\caption{Filtering example}
\label{filtering}
\end{figure}

\begin{example}
Assume $x, y$, two variables with domains $dom(x) = \{2, 3, 4, 5, 6\}$ and $dom(y) = \{0, 1, 2, 3, 4\}$. Two constraints hold on those variables. $c_1 \equiv x < y$ and $c_2 \equiv x > 2$. The process lasts as long as a constraint can remove a value. 
At the end of the filtering process described in Figure \ref{filtering}, we can see that the solution has already been found. This is not the case for all problems and, most of the time, we need the second step of the search described hereafter.\\
\label{pruning}
\end{example}

The filtering algorithm can be implemented with several different consistency modes: the bound consistency or the domain consistency. In the later, a value is kept in the domain if and only if it participates in a least one solution while in the bound consistency, only the bounds (lower and upper frontier of the domain) are required to satisfy their constraints. In example \ref{pruning} as both modes achieve the same results, it will be more efficient to use the bound consistency since less testing will be executed. Note that since the bound consistency only validates the frontier of the domain, it might keep inconsistent values. Example \ref{consistency} is bound consistent (will not filter values anymore), but not domain consistent (can still filter on some values).

\begin{example}
\label{consistency}
Assume $x, y$, two variables with domains $dom(x) = \{2, 3, 4, 5, 6\}$ and $dom(y) = \{2, 6\}$. One constraint holds on those variables. $c_1 \equiv x + y = 8$. 
\end{example}

\begin{figure}[!b]
\centering
\begin{tikzpicture}[level distance=1cm,
level 1/.style={sibling distance=4cm},
level 2/.style={sibling distance=3cm},
level 3/.style={sibling distance=2cm}]
\tikzstyle{every node}=[circle,draw]
\node (Root) {}%[red] {}
    child {
    node {}
    child {
		node [draw=none] {} edge from parent[dashed]
	}
	child {
		node [draw=none] {} edge from parent[dashed]
	}  
     edge from parent node [left,draw=none] { $x_1 = v_1$}
}
child {
    node {}
    child {
    	node {}
    	child {
			node [draw=none] {} edge from parent[dashed]
		}
		child {
			node [draw=none] {} edge from parent[dashed]
		} 
    	edge from parent node [left,draw=none] { $x_1 = v_2$}
    }
    child {
    	node {}
    	child {
    		node{  } 
    		child {
				node [draw=none] {} edge from parent[dashed]
			}
			child {
				node [draw=none] {} edge from parent[dashed]
			} 
    		edge from parent node [left,draw=none] { $x_1 = v_3$}
    	}
    	child {
    		node{  }
    	    child {
				node [draw=none] {} edge from parent[dashed]
			}
			child {
				node [draw=none] {} edge from parent[dashed]
			} 
    		edge from parent node [right,draw=none] { $x_1 \neq v_3$}
    		}
    	 edge from parent node [right,draw=none] { $x_1 \neq v_2$}
    }
      edge from parent node [right,draw=none] { $x_1 \neq v_1$}
};

\end{tikzpicture}
\caption{Binary Branching}
\label{branching}
\end{figure}

\item The second step is \textbf{the branching (or search)}. In this phase, we will explore the search space. Since the search space can be represented as a tree, three decisions have to be made: the branching strategy, the variable/value/branch selection heuristic and the exploration strategy. A popular branching procedure is the binary labelling: at each non-terminal node, it creates two branches: one assigns a value to an unbounded variable, the other rejects the value from its domain. A trendy variable selection heuristic is the minimum remaining values (MRV) choosing the variables whose domain comports the smallest number of (remaining) values (fail-first). The search strategy defines then the order the branches are visited; backtracking allows us to explore both side of the search tree.\\
\end{itemize}

\section{Constraint Optimization Problems (COP)}

The Constraint Optimization Problems are a specific case of Constraint Satisfaction Problems extended by an optimization function $f\mathcal(X)$ (and hence modelled quite similarly). The goal changes from the research of one (or all) valid solution(s) to the pursuit of an optimal solution with regard to $f\mathcal(X)$. Accordingly, once the first solution is found, a constraint enforcing the objective score of the next solution to be strictly higher (in case of maximization) than the ongoing best objective is included. Each time a new solution is found (which will be better thanks to the new constraint), the value of the objective is updated. For the case of an maximization problem, the constraint can be expressed as $c_{opt} \equiv f(\mathcal{X}) > currentBest$ and each time a solution is found, we update $currentBest = f(\mathcal{X})$\\

\section{Global Constraints}

As stated in \citep{toBorNotToBe}:
\begin{quote}
[...] a constraint C is often called “global” when “processing” C as a whole gives better results than “processing” any conjunction of constraints that is “semantically equivalent” to C
\end{quote}
More strictly, C. Bessière and P. Van Hentenryck define three notions of globality: the semantic, operational and algorithmic ones. We are here interested by the operational globality since both the \texttt{AllDifferent} and \texttt{GCC} constraints can be decomposed into a clique of simple arithmetic constraints \citep{alddifGccDecomp}. More intuitively, we can understand a global constraint as  a constraint holding on an unspecified number of variables. We explain the two global constraints we use in the modelling of the EPL's problem: the \texttt{AllDifferent} and \texttt{GCC} constraints.\\

\subsection{All Different Constraint}

The \texttt{AllDifferent} is an often used constraint defined as holding if and only if all the variables participating in its scope are assigned different values \citep{allDiffDef2} (a formal definition can be found in \citep{allDiffDef}).

\begin{example}
Let's take the square drawn hereafter (Figure \ref{AllDiff}). There are 9 variables, represented by tiles. All variables have the domain the set \{1,2,3\}. The only existing constraint is an \texttt{AllDifferent} on each row, and on each column.

\begin{figure}
\centering
\begin{tikzpicture}[xscale=1,transform shape]

\foreach \xx in{0,1,...,3}{
\draw[very thick](\xx * 2,0)--(\xx  *2,6);
\draw[very thick](0,\xx * 2)--(6,\xx  *2);
}


\draw (1,1.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}} \}$};

\draw (1,5.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}}\}$};

\draw (3,1.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}} \}$};
\draw (3,3.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}} \}$};

\draw (5,1.8) node[below, scale=0.7]{$\{ \textcolor{red}{1, 2}, 3 \}$};
\draw (5,3.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}} \}$};
\draw (5,5.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}} \}$};


\draw (1,3) node[scale=2]{3};
\draw (3,5) node[scale=2]{3};


       \draw[red, very thick, ->](6.5,3)--(7.5, 3);


\foreach \xx in{0,1,...,3}{
\draw[very thick](8 + \xx * 2,0)--(8 + \xx  *2,6);
\draw[very thick](8 + 0,\xx * 2)--(8 + 6,\xx  *2);
}


\draw (8 + 1,1.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}} \}$};

\draw (8 + 1,5.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}}\}$};

\draw (8 + 3,1.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}} \}$};
\draw (8 + 3,3.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}} \}$};


\draw (8 + 5,3.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}} \}$};
\draw (8 + 5,5.8) node[below, scale=0.7]{$\{ 1, 2, \textcolor{red}{\cancel{3}} \}$};


\draw (8 + 1,3) node[scale=2]{3};
\draw (8 + 3,5) node[scale=2]{3};
\draw (8 + 5,1) node[scale=2]{\textcolor{red}{3}};


\end{tikzpicture}
\caption{AllDifferent constraint}
\label{AllDiff}
\end{figure}
\end{example}


With the assignment on the left part, we can observe the filtering on the other variables' domains. Since the \texttt{AllDifferent} takes the entire row (resp. column) into account, bound/domain consistency process will detect that an assignment of the bottom-right tile to either 1 or 2 would render the problem infeasible. The value is accordingly assigned to 3 and the filtering finishes (there are no more values to remove from the domains)\\

As stated above, the \texttt{AllDifferent} if not operationnaly global: the obtained filtering would not have been possible with a decomposition of the constraint in a clique of binary constraints (domain of last tile would still be $\{ 1, 2, 3\}$).\\


\pagebreak
\subsection{Global Cardinality Constraint (GCC)}

The GCC is a generalization of the previous constraint in the sense that it does not enforce uniqueness of a value in the variables (of its scope) but rather enforces that the value $i$ occurs between $l_i$ and $u_i$ times for the variables participating in its scope \citep{alddifGccDecomp}.

\begin{example}
Let's consider 6 students. Each of them is represented by a tile on the Figure \ref{GCC}. They all need to present an oral examination which is given on three different slots (1, 2 and 3). For each student, the variable represents the slot in which he will present the oral examination. Since the slot lasts only an hour, no more than two students are allowed in an examination slot. This will be expressed by the GCC constraint. It will receive the array of all the variables $X \equiv x_i \; \vert \; \forall i \in {0, \dots, 5}$, the array of $l_i \equiv [0, 0, 0]$ and the array of $u_i \equiv [2, 2, 2]$.

\begin{figure}
\centering
\begin{tikzpicture}[xscale=1,transform shape]

\foreach \xx in{0,1,...,6}{
\draw[very thick](\xx * 2,0)--(\xx  *2,2);
}

\foreach \xx in{0,1,...,5}{
\draw (\xx  *2 +1 ,1.9) node[below, scale=0.7]{$\{ 1, 2, 3 \}$};
}

\draw[very thick](12,0)--(0, 0);
\draw[very thick](0,2)--(12, 2);


%%%%

       \draw[red, very thick, ->](6,-0.5)--(6, -1.5);
       \draw (6, -1) node[right]{\textcolor{red}{Possible assignment}};
       
       %%%%


\foreach \xx in{0,1,...,6}{
\draw[very thick](\xx * 2,-4)--(\xx  *2,-2);
}


\draw (0 +1 ,-3) node[scale =2]{1};
\draw (1  *2 +1 ,-3) node[scale =2]{1};
\draw (2  *2 +1 ,-3) node[scale =2]{2};
\draw (3  *2 +1 ,-3) node[scale =2]{2};
\draw (4  *2 +1 ,-3) node[scale =2]{3};
\draw (5  *2 +1 ,-3) node[scale =2]{3};



\draw[very thick](12,-4)--(0, -4);
\draw[very thick](0,-2)--(12, -2);

\end{tikzpicture}
\caption{GCC constraint}
\label{GCC}
\end{figure}
\end{example}