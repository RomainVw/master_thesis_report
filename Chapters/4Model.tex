\lhead{\vspace{1em}\emph{LINGI2990 - Master Thesis}}
\chapter{Model} 
\label{model}
In this chapter, we will describe the model we implemented, the variables we used, their domains, and the constraints that hold them. We begin by introducing the simple model and follow on to its extension.\\

\section{Parameters of the problem}
We begin by introducing the general parameters describing the problem then move on to the courses specific ones. The students, professors and constraints parameters are finally introduced and precede the modelling variables used in the solver.\\


\subsection{The session}
First, let us begin with the general parameters: any examination session is characterized by the number of days it spans and the number of possible slots per day (e.g. the maximum number of different exams a student can have in the same day). For modelling purposes, we define the total number of sessions which is simply the product of the two former variables.
\begin{verbatim}
/* session values */
nDays           // Is the number of days of the session
nSlotPerDay     // Is the number of slots in a day. If we want an
                // examination scheduled either in the morning
                // or in the afternoon we set this variable to 2.
nSlots          // Is the number of slots in the entire session
                // =  nDays * nSlotPerDay

/* Ranges */
rDays  = {0 until nDays}    // Is the range of all days in the session
rSlots = {0 until nSlots}   // Is the range of all slots in the session
\end{verbatim}

Since no examination can be scheduled on Sundays, we filter out any corresponding values from \texttt{rDays} and \texttt{rSlots} and store the new ranges in two parameters: the \texttt{availableDays} and the \texttt{availableSlots}.

\begin{verbatim}
availableDays  = rDays  \ Sundays       // Is the range of all days where
                                        // an examination can be scheduled
availableSlots = rSlots \ SundaysSlots  // Is the range of all slots where
                                        // an examination can be scheduled\\
\end{verbatim}

\subsection{The courses}

In the EPL's case of exam scheduling, a course can be (exclusively) of one of the three following categories: Unislot, Multislot or Oral. In order to support the different categories, we introduce a new type \texttt{subcourse} whose role is to represent the different slots needed by a course to be scheduled.

\begin{mydef}
(Course). A course is a non empty set of subcourses.
\end{mydef}

\begin{mydef}
(Subcourse). Atomic building bloc of a course, each subcourse represents one slot taken by the course examination.
\end{mydef}

\begin{notation}
(Subcourse notation). $courseLabel^{slotID}$ where $courseLabel$ is the label of the course and $slotID$ is the slot position of the subcourse.
\end{notation}

\begin{mydef}
\textit{(Courses).} $Courses$ is the set of all the courses.
\end{mydef}

\begin{example}
$LINGI2990 \in Courses$ needs 4 slots for its examination, it will then be composed of 4 courses represented as: \[LINGI2990 \equiv \lbrace LINGI2990^1,\; LINGI2990^2,\; LINGI2990^3,\; LINGI2990^4 \rbrace\]
\end{example}

\begin{mydef}
(Subcourses). $Subcourses$ is the set of all subcourses.
\end{mydef}

\begin{example} 
\[Subcourses_{LINGI2990} \equiv\; \lbrace LINGI2990^1,\; LINGI2990^2,\; LINGI2990^3,\; LINGI2990^4 \rbrace\]
\end{example}

\begin{mydef}
(Unislots, Orals and Multislots). $Unislots$, $Orals$ and $Multislots$ are sets of courses respectively containing only courses of category Unislot, Oral and Multislot. Three notes are worth mentioning: \begin{itemize}
\item Since the examination rules of the EPL state that a professor can not ask a student to make two performances for the same course on different days of the session:
\[ \forall course \in Multislots : \vert course \vert \leq nSlotPerDay\]
\item Since by definition unislot courses only require one slot: 
\[ \forall course \in Unislots : \vert course \vert = 1\]
\item Since the membership to one of the category is exclusive: 
\begin{itemize}
\item $ Unislots \cap Orals = \emptyset $
\item $ Unislots \cap Multislots = \emptyset $
\item $ Orals    \cap Multislots = \emptyset $
\end{itemize}
\end{itemize}
\end{mydef}

From the definitions here-above, we can deduce that: \[ Courses = Unislots \cup Orals \cup Multislots \]

\begin{example}
Suppose $LINGI2262$ and $SINF2275$ are two courses of the oral category needed respectively 4 and 2 slots. Then 

\begin{tabular}{rl}
$Orals$ & $\equiv \{ LINGI2262, SINF2275  \}$ \\ 
  		& $\equiv \lbrace \lbrace LINGI2262^1,\; LINGI2262^2,\; LINGI2262^3,\; LINGI2262^1 \rbrace,$\\
  		& $\;\;\;\;\;\; \lbrace SINF2275^1,\; SINF2275^2 \rbrace \rbrace$ \\ 
\end{tabular}
\end{example}

\begin{mydef}
(Written). $Written \equiv Unislots \cup Multislots$\\
\end{mydef}

\subsection{The students}

\begin{mydef}
(Students). $Students$ is the set of all student that are enrolled in at least one examination during the session.
\end{mydef}

\begin{mydef}
(enrolled). $enrolled$ is the set of all pairs (student, course) such that the student is registered in the course. If the course is a multislot, the student is enrolled in all subcourses of the course, while if it is an oral, the student is only enrolled in the course (and not in all subcourses of the oral).\\
\end{mydef}

\subsection{The professors}

\begin{mydef}
(Professors). $Professors$ is the set of all professors giving a course in which at least one student is enrolled.
\end{mydef}

\begin{mydef}
(teachesWritten). $teachesWritten$ is the set of all pair (professor, subcourse) such that the professors gives the course corresponding to subcourse (and the course needs a written examination).
\end{mydef}

\begin{mydef}
(teachesOral). $teachesOral$ is the set of all pair (professor, subcourse) such that the professors gives the course corresponding to subcourse (that the course needs an oral examination).\\
\end{mydef}

\subsection{The constraints parameters}

\begin{mydef}
(assignment). $assignment$ contains pairs of (subcourse, slot) representing the assignment of a specific exam to a specific slot. Those values represent courses that need to be scheduled on a specific date for external reasons. (e.g. a course scheduled by the end user: the examination needs to be held the first/last day of the session, the examination needs really specific material which is only available on a specific day, \dots).
\end{mydef}

\begin{mydef}
(restriction). $restriction$ contains pairs of (subcourse,slot) representing the restriction for a specific exam to happen on a specific slot. Those values represent courses that can't be scheduled on specific dates due to external reasons. (e.g. the professor is unavailable on specifics days, the specific material isn't available on some days, \dots).\\
\end{mydef}

\begin{mydef}
(nStudentPerSlot). $nStudentPerSlot$ contains, for each course that is an oral, the maximum number of students admissible in the same slot. \\
\end{mydef}

\section{Variables and their Domain}

\begin{variable}
(date). $date$ is the set of variables representing slots scheduled for at least one subcourse.\\
 $date$ associates to all subcourses a variable corresponding to the slot assigned for the corresponding examination. Each of those variable has a domain that is the set of all available slots. \[
\forall course \in Courses, \; \forall subcourse \in course : \quad date_{subcourse} \in availableSlots
\]
\end{variable}

\begin{variable}
(studentDate). $studentDate$ is the set of variables representing slots scheduled for at least one course followed by a student.\\
 $studentDate$ associates to each pair of (student, course) (such that student is enrolled in course) a variable corresponding to the slot in which the student will present the examination of his course. If the examination is written, all students inscribed in this examination will present the exam at the same slot(s) (which is the $date_{course}$). But if the  examination is an oral one, each student will present their own oral examination in a specific slot at which the examination is given.
\[
\forall (student, course) \in enrolled \quad : \quad studentDate_{(student,course)} \in  availableSlots
\]
\end{variable}

\begin{notation} $date_{course} = \lbrace \forall subcourse \in course, date_{subcourse} \rbrace$
\end{notation}

\section{Constraints}

Now that the basic variables of the model have been laid out, we describe all the constraints that hold on previously defined variables.

\begin{constraint}
[No conflict]\\
Each student has all his exams in different slots.
\begin{multline}
\forall student \in Students : AllDifferent(courseSet)\\
with \; courseSet \equiv \{studentDate_{course} \quad | \quad (student,course) \in enrolled \}
\end{multline}
\end{constraint}

In order to improve the resilience of the solver, we define this constraint as a soft constraint. A soft global constraint is basically a extension of its hard version with a new cost variable representing its degree of violation \citep{diffandequalTaxonomy}. A definition of the variable-based, minimisation soft all different can also be found in \citep{diffandequalTaxonomy}. The violation cost is then entered in the optimisation function in order to be minimized.

\begin{constraint}[Linking orals for student with oral exams]\\
For each oral exam, the slot in which the student presents the exam must be a slot where the exam is scheduled.
\begin{multline}
\forall course \in Orals : \forall \; \{ student \quad | \quad (student,course) \in enrolled \; \}  :  \\
 studentDate_{(student,course)} \in date_{course}
\end{multline}
\end{constraint}


\begin{constraint}[All slots for a multislot exam must be contiguous]\\
\begin{equation}
\forall course \in Multislots : \forall i  \in \{1, \dots, \vert course \vert - 1\} :
date_{course^{i+1}} = date_{course^i} + 1
\end{equation}
\end{constraint}

\begin{constraint}[All slots for a multislot exam must be held in the same day]\\
The verification can be done only by looking if the first and last subcourse are held on the same day.

\begin{multline}
\forall course \in Multislots  :\\
date_{course^1} \; / \; nSlotPerDay = date_{course^n} \; /  \; nSlotPerDay
\end{multline}
where $/$ is the integer division mapping a slot to a day and $n = \vert course \vert$, the number of slots required by the multislot.
\end{constraint}

\begin{constraint}[All slots for an oral exam must be different]\\
For each course needing an oral examination, all the slots scheduled for said course must be different
\begin{equation}
\forall course \in Orals : AllDifferent(course)
\end{equation}

Note : We may be tempted to replace this constraint by a more restrictive one: \[\forall i, \forall course \in Orals date_{course^i} < date_{course^{i+1}} \] However, this new constraint could not hold simultaneously with the following constraint without modifying the signification of the model. Indeed, since next constraint allows us to fix a subcourse to a specific slot, it would implicitly express that all other slots needs to be scheduled \textit{after} the assigned slot.
\end{constraint}

\begin{constraint}[Slot assignment]\\
Some exams can be preassigned to specific slots. Those pre-assignments are stored in the $assignment$ set. \\
\begin{equation}
\forall  (subcourse, slot)  \in  assignment : date_{subcourse} = slot
\end{equation}
\end{constraint}

\begin{constraint}[Slot restrictions]\\
Some exams can be excluded from specific slots. Those restrictions are stored in the $restriction$ set.\\
\begin{equation}
\forall  (course, slot)  \in  restriction, \forall d \ in date_{course}: d \neq slot
\end{equation}
\end{constraint}

\begin{constraint}[Full availability of the professor for an oral exam]\\
Professors need to be present at their oral examination and they cannot be available somewhere else. It is thus needed to have no other examination for a professor at the same time as one of his/her oral examinations.
\begin{multline}
\forall professor \in Professors :
\forall (professor,subcourse) \in teachesWritten :\\
date_{subcourse} \not\in date_{oralSet} \\
with \; oralSet \equiv \{date_{course} \quad | \quad (professor, course) \in teachesOral \; \}
\end{multline}
\end{constraint}

\begin{constraint}[Maximum capacity for an oral slot]\\
Each oral exam has a specific number of slots. Each slot has a maximum capacity. The number of students scheduled to a specific slot cannot exceed this limit.
\begin{multline}
\forall oral \in  Orals, \forall suboral \in oral : \\
\sum_{student} \left( studentDate_{(student, oral)} == date_{suboral} \right)
\in \lbrace 0\;, \dots, \;nstudentPerSlot(oral)\rbrace
\end{multline}
\end{constraint}


% Followings are part of the optimization

%\textbf{Constraint 8 - Number of session between each pair of courses}\\
%\textbf{Constraint 9 - Minimal session gap}\\
\section{Extension of the model}

In order to represent the reality more faithfully, we need to extend the ongoing model. More than slots, the real key of the problem is the number of days the students have between each of their exams. We define here new variables and constraints built on the basic model.

\subsection{Variables and their Domain}
\begin{variable}
(dayDate). $dayDate$ is the set of variables representing days scheduled for at least one subcourse\\
$dayDate$ associates to all courses a variable corresponding to the day assigned for the corresponding examination. Each of those variable has a domain that is the set of all available days.
\[
\forall course \in Courses  :  dayDate_{course} \in availableDays
\]
\end{variable}


\begin{variable}
(studentDayDate). $studentDayDate$ is the set of all the variables representing days in which a student presents his examination.\\
$studentDayDate$ associates to each pair of (student, course) (such that student is enrolled in course) a variable corresponding to the day in which the student will present the examination of his course. If the examination is a written examination, all student inscribed in this examination will present the exam at the same day (which is the $dayDate_{course}$). But if the  examination is an oral one, all students will present their own oral examination in different days during which the examination is given.
\[
\forall (student, course) \in enrolled \quad : \quad studentDayDate_{(student,course)} \in  availableDays
\]
\end{variable}

\subsection{Constraints}
Those new variables imply new constraints on top of those previously defined.\\


\begin{constraint}[Linking basic variables with new ones.]\\
To better understand how the linking will be done, let's take a simple example: a session of 7 days, and having 2 slots per day. The Figure \ref{dayLinking} illustrates the matching between slotsID and daysID.

\begin{figure}[!h]
\centering
\begin{tikzpicture}[xscale=1,transform shape]

% slot ID
\draw[dashed]  (0,1.2) node[below ]{SlotID};
\foreach \xx in{0,1,...,11}{
\draw[dashed]  (\xx + 2,1.2) node[below ]{\xx};
}

% Day ID
\draw[dashed]  (0,0.4) node[below ]{DayID};
\foreach \xx in{0,1,...,5}{
\draw [decorate, decoration={brace,mirror,raise=1mm}, color=black, very thick] (\xx * 2 + 2,0.7) - - (\xx * 2 + 3,0.7) ;
\draw[dashed]  (\xx * 2 + 2.5,0.4) node[below ]{\xx};
}

\end{tikzpicture}
\caption{Linking slotIDs with dayIDs}
\label{dayLinking}
\end{figure}

\begin{multline}
\forall course \in Courses, \forall subcourse \in course: 
date_{subcourse} \in \\ [dayDate_{subcourse}\ *\  nSlotPerDay,\; \dots, \;  (dayDate_{subcourse} + 1)\ *\  nSlotPerDay[
\end{multline}

\begin{multline}
\forall (student, course) \in enrolled, \forall subcourse \in course :
studentDate_{subcourse} \in \\ [studentDayDate_{subcourse}\ * nslotPerDay,\; \dots,\\
 \;  (studentDayDate_{subcourse} + 1)\ *\  nSlotPerDay[
\end{multline}

\end{constraint}



\section{Optimization}
Now that we have defined all the constraints that our model needs to satisfy, and since our problem is a COP, we move on to the definition of the objective function(s). Several sub-objectives have been implemented and are described below. All of those try to maximize the comfort of students.\\

\begin{mydef}
(The Global Minimal Gap (GMG)). The Global Minimal Gap (GMG) is the minimal gap (number of days or slots) that can be found for a student participating in the session. $GMG \equiv \min \left(studentsMinimalgap\right)$\\
with $studentsMinimalGap \equiv min(\lbrace \forall student \in Students, minimalGap(student) \rbrace)$ where minimalGap(student) is the minimum value of the matrix representing the differences between the scheduled slots (or days) of the courses (and subcourses) the student is enrolled in.
\end{mydef}

\paragraph{Sub-Objective 1 - Having the smallest global minimal gap (GMG)\\} All students have gaps betweens their examinations (i.e. the number of free slots - or days). For every student, we will retrieve the smallest gap, and then retrieve the smallest gap among all students. This GMG will be the first value that we will optimize since it improves the comfort of all students globally. We ensure this way that the global comfort is improved before the individual one. All gaps can be expressed either in terms of slots (the number of free slots), or in terms of days. If the GMG is expressed in terms of slots, we will speak about the GMSG (Global Minimal Slot Gap), and if it's expressed in terms of days, we will speak about GMDG. Most of the time, we will use the GMDG (Global Minimal Day Gap) since students care about the number of days between their examinations (rather than the number of slots).\\


\paragraph{Sub-Objective 2 - Having as few students as possible in the GMG\\}
Once the GMG has optimized enough, we begin the optimization on the number of students having the GMG as their own minimal gap (for the search strategies, please refer to chapter \ref{solver}, section \ref{solverSearch}). It may be physically not possible for some students to have a better minimal gap than the GMG however we still want to improve, if possible, the comfort of the other students.\\

\paragraph{Sub-Objective 3 - Trying to improve the minimal gap of all remaining students\\}
With the optimization of the two previous Sub-Objectives, we typically have the distribution of students based on their minimal gap as represented here under (Figure \ref{day_distribution}). We will try to push this distribution as much as possible to the right (in other words, try to increase the minimal gap between 2 exams for each student).

\begin{figure}[!h]
\centering
\begin{tikzpicture}
\begin{axis}[ybar interval,
every axis/.append style={font=\tiny},
xlabel = {Minimal Gap $\left[ days\right]$},
ylabel = {\# students}
	]
\addplot coordinates
	{(0,0) (1,94) (2,216) (3,54)(4,20)(5,11)(6,13)(7,13)(8,2)(9,6)
	(10,0)(11,2)(12,0)(13,2)(14,1)(15,0)(16,0)(17,1)(18,0)(19,0)(20,1)(21,1)};
	\node[anchor=west] (source) at (axis cs:3,125){};
       \node (destination) at (axis cs:7,125){};
       \draw[red, very thick, ->](source)--(destination);
\end{axis}
\end{tikzpicture}
\caption{Barplot of number of students based on their minimal gap}
\label{day_distribution}
\end{figure}

\newpage



